# Copies source code and build webapp
FROM node:14.17.5 as builder
WORKDIR /app
COPY package.json yarn.lock ./
RUN yarn install


ARG IMAGE_TAG_ARG='m-default'
ENV REACT_APP_IMAGE_TAG=${IMAGE_TAG_ARG}
RUN echo REACT_APP_VERSION=${IMAGE_TAG_ARG} >> .env.local
COPY . ./
RUN yarn build

# Serve webapp
FROM nginx:1.15.5-alpine as webapp
WORKDIR /app
COPY --from=builder /app/build /app/www
COPY etc/nginx.conf /etc/nginx/conf.d/default.conf
CMD ["nginx", "-g", "daemon off;"]
